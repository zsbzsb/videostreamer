#ifndef DATAFRAME_CPP
#define DATAFRAME_CPP

#include "include/DataFrame.h"

	DataFrame::DataFrame(uint8_t* NewDataBuffer, uint32_t NewFrameWidth, uint32_t NewFrameHeight) : VideoDataBuffer(nullptr), AudioDataBuffer(nullptr), BufferSize(NewFrameWidth * NewFrameHeight * 4), IsVideoData(true), FrameWidth(NewFrameWidth), FrameHeight(NewFrameHeight), IsAudioData(false), AudioChannelCount(0), AudioSampleRate(0)
	{
		VideoDataBuffer = new uint8_t[NewFrameWidth * NewFrameHeight * 4];
		std::memcpy(VideoDataBuffer, NewDataBuffer, sizeof(uint8_t) * NewFrameWidth * NewFrameHeight * 4);
	};
	DataFrame::DataFrame(uint32_t NewFrameWidth, uint32_t NewFrameHeight, uint8_t R, uint8_t G, uint8_t B, uint8_t A) : VideoDataBuffer(nullptr), AudioDataBuffer(nullptr), BufferSize(NewFrameWidth * NewFrameHeight * 4), IsVideoData(true), FrameWidth(NewFrameWidth), FrameHeight(NewFrameHeight), IsAudioData(false), AudioChannelCount(0), AudioSampleRate(0)
	{
		VideoDataBuffer = new uint8_t[NewFrameWidth * NewFrameHeight * 4];
		uint32_t i = 0;
		for (uint32_t y = 0; y <  NewFrameHeight; y++)
		{
			for (uint32_t x = 0; x < NewFrameWidth; x++)
			{
				*(VideoDataBuffer + i) = R;
				*(VideoDataBuffer + i + 1) = G;
				*(VideoDataBuffer + i + 2) = B;
				*(VideoDataBuffer + i + 3) = A;
				i += 4;
			}
		}
	};
	DataFrame::DataFrame(uint8_t* NewAudioBuffer, uint32_t NewSampleCount, uint32_t NewAudioChannelCount, uint32_t NewAudioSampleRate) : VideoDataBuffer(nullptr), AudioDataBuffer(nullptr), BufferSize(NewSampleCount * NewAudioChannelCount), IsVideoData(false), FrameWidth(0), FrameHeight(0), IsAudioData(true), AudioChannelCount(NewAudioChannelCount), AudioSampleRate(NewAudioSampleRate)
	{
		AudioDataBuffer = new int16_t[NewSampleCount * NewAudioChannelCount];
		int16_t* sourcearray = (int16_t*)NewAudioBuffer;
		std::memcpy(AudioDataBuffer, sourcearray, sizeof(uint16_t) * NewSampleCount * NewAudioChannelCount);
	};
	DataFrame::~DataFrame()
	{
		delete[] VideoDataBuffer;
		delete[] AudioDataBuffer;
	};

#endif