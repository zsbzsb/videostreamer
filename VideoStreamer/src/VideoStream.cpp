#ifndef VIDEOSTREAM_CPP
#define VIDEOSTREAM_CPP

#include "include/VideoStream.h"

	VideoStream::VideoStream(const char* FilePath, int* ReturnValue, bool ForceOptions, bool EnableVideo, bool EnableAudio) : HasVideo(EnableVideo), HasAudio(EnableAudio), EndOfFile(false), VideoStreamID(-1), AudioStreamID(-1), VideoLength(1), FormatContext(nullptr), VideoCodecContext(nullptr), AudioCodecContext(nullptr), VideoCodec(nullptr), AudioCodec(nullptr), VideoRawBuffer(nullptr), VideoRGBABuffer(nullptr), AudioRawBuffer(nullptr), AudioPCMBuffer(nullptr), VideoSWContext(nullptr), AudioSWContext(nullptr), OutputChannelCount(0)
	{
		av_register_all();
		if (avformat_open_input(&FormatContext, FilePath, NULL, NULL) != 0)
		{
			*ReturnValue = 1;  // Unable to load file
			return;
		};
		if (avformat_find_stream_info(FormatContext, NULL) < 0)
		{
			*ReturnValue = 2; // Unable to find stream information
			return;
		};
		for (unsigned int i = 0; i < FormatContext->nb_streams; i++)
		{
			if (FormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO && VideoStreamID == -1)
			{
				VideoStreamID = i;
			}
			if (FormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO && AudioStreamID == -1)
			{
				AudioStreamID = i;
			}
			if (VideoStreamID != -1 && AudioStreamID != -1)
			{
				break;
			}
		}
		if (HasVideo)
		{
			if (VideoStreamID == -1)
			{
				HasVideo = false;
				if (ForceOptions)
				{
					*ReturnValue = 3; // Unable to find a video stream
					return;
				}
			}
		}
		if (HasAudio)
		{
			if (AudioStreamID == -1)
			{
				HasAudio = false;
				if (ForceOptions)
				{
					*ReturnValue = 4; // Unable to find audio stream
					return;
				}
			}
		}
		if (HasVideo)
		{
			VideoCodecContext = FormatContext->streams[VideoStreamID]->codec;
			if (!VideoCodecContext)
			{
				HasVideo = false;
				if (ForceOptions)
				{
					*ReturnValue = 5; // Unable to get the video codec context
					return;
				}
			}
		}
		if (HasVideo)
		{
			VideoCodec = avcodec_find_decoder(VideoCodecContext->codec_id);
			if (VideoCodec == NULL)
			{
				HasVideo = false;
				if (ForceOptions)
				{
					*ReturnValue = 6; // Unable to find the video codec
					return;
				}
			}
		}
		if (HasAudio)
		{
			AudioCodecContext = FormatContext->streams[AudioStreamID]->codec;
			if (!AudioCodecContext)
			{
				HasAudio = false;
				if (ForceOptions)
				{
					*ReturnValue = 7; // Unable to get the audio codec context
					return;
				}
			}
		}
		if (HasAudio)
		{
			AudioCodec = avcodec_find_decoder(AudioCodecContext->codec_id);
			if (AudioCodec == NULL)
			{
				HasAudio = false;
				if (ForceOptions)
				{
					*ReturnValue = 8; // Unable to find the audio codec
					return;
				}
			}
		}
		if (HasVideo)
		{
			if (avcodec_open2(VideoCodecContext, VideoCodec, NULL) != 0)
			{
				HasVideo = false;
				if (ForceOptions)
				{
					*ReturnValue = 9; // Unable to load video codec
					return;
				}
			}
		}
		if (HasAudio)
		{
			if (avcodec_open2(AudioCodecContext, AudioCodec, NULL) != 0)
			{
				HasAudio = false;
				if (ForceOptions)
				{
					*ReturnValue = 10; // Unable to load audio codec
					return;
				}
			}
		}
		if (FormatContext->duration != AV_NOPTS_VALUE) VideoLength = FormatContext->duration;
		EndOfFile = false;
		if (HasVideo)
		{
			VideoRawBuffer = CreateVideoBuffer(VideoCodecContext->pix_fmt, VideoCodecContext->width, VideoCodecContext->height);
			VideoRGBABuffer = CreateVideoBuffer(PIX_FMT_RGBA, VideoCodecContext->width, VideoCodecContext->height);
			if (VideoRawBuffer == nullptr || VideoRGBABuffer == nullptr)
			{
				HasVideo = false;
				if (ForceOptions)
				{
					*ReturnValue = 11; // Unable to create video buffers
					return;
				}
			}
		}
		if (HasAudio)
		{
			AudioRawBuffer = avcodec_alloc_frame();
			if (!AudioRawBuffer || av_samples_alloc(&AudioPCMBuffer, NULL, AudioCodecContext->channels, av_samples_get_buffer_size(NULL, AudioCodecContext->channels, MAXSAMPLES, AVSampleFormat::AV_SAMPLE_FMT_S16, 0), AVSampleFormat::AV_SAMPLE_FMT_S16, 0) < 0)
			{
				HasAudio = false;
				if (ForceOptions)
				{
					*ReturnValue = 12;  // Unable to create audio buffers
					return;
				}
			}
			else
			{
				avcodec_get_frame_defaults(AudioRawBuffer);
			}
		}
		if (HasVideo)
		{
			int swapmode = SWS_FAST_BILINEAR;
			if (VideoCodecContext->width * VideoCodecContext->height <= 500000 && VideoCodecContext->width % 8 != 0) swapmode |= SWS_ACCURATE_RND;
			VideoSWContext = sws_getCachedContext(NULL, VideoCodecContext->width, VideoCodecContext->height, VideoCodecContext->pix_fmt, VideoCodecContext->width, VideoCodecContext->height, PIX_FMT_RGBA, swapmode, NULL, NULL, NULL);
		}
		if (HasAudio)
		{
			AudioSWContext = swr_alloc();
			uint64_t inchanlayout = AudioCodecContext->channel_layout;
			if (inchanlayout == 0) inchanlayout = av_get_default_channel_layout(AudioCodecContext->channels);
			uint64_t outchanlayout = inchanlayout;
			if (outchanlayout != AV_CH_LAYOUT_MONO) outchanlayout = AV_CH_LAYOUT_STEREO;
			av_opt_set_int(AudioSWContext, "in_channel_layout", inchanlayout, 0);
			av_opt_set_int(AudioSWContext, "out_channel_layout", outchanlayout, 0);
			av_opt_set_int(AudioSWContext, "in_sample_rate", AudioCodecContext->sample_rate, 0);
			av_opt_set_int(AudioSWContext, "out_sample_rate", AudioCodecContext->sample_rate, 0);
			av_opt_set_sample_fmt(AudioSWContext, "in_sample_fmt", AudioCodecContext->sample_fmt, 0);
			av_opt_set_sample_fmt(AudioSWContext, "out_sample_fmt", AV_SAMPLE_FMT_S16, 0);
			swr_init(AudioSWContext);
			OutputChannelCount = av_get_channel_layout_nb_channels(outchanlayout);
		}
		if (!HasVideo && !HasAudio)
		{
			*ReturnValue = 13;
			return;
		}
		else
		{
			*ReturnValue = 0;
			return;
		}
	};
	VideoStream::~VideoStream()
	{
		if (VideoCodecContext != NULL) avcodec_close(VideoCodecContext);
		if (AudioCodecContext != NULL) avcodec_close(AudioCodecContext);
		if (VideoRawBuffer != NULL) av_free(VideoRawBuffer);
		if (VideoRGBABuffer != NULL) av_free(VideoRGBABuffer);
		if (AudioRawBuffer != NULL) avcodec_free_frame(&AudioRawBuffer);
		if (AudioPCMBuffer != NULL) av_free(AudioPCMBuffer);
		if (VideoSWContext != NULL) sws_freeContext(VideoSWContext);
		if (AudioSWContext != NULL) swr_free(&AudioSWContext);
		if (FormatContext != NULL) av_close_input_file(FormatContext);
	}
	AVFrame* VideoStream::CreateVideoBuffer(enum PixelFormat CurrentPixelFormat, int CurrentWidth, int CurrentHeight)
	{
		AVFrame* picture;
		uint8_t* picturebuffer;
		picture = avcodec_alloc_frame();
		if (!picture) return nullptr;
		int size = avpicture_get_size(CurrentPixelFormat, CurrentWidth, CurrentHeight);
		picturebuffer = (uint8_t*)av_malloc(size);
		if (!picturebuffer)
		{
			av_free(picture);
			return nullptr;
		}
		avpicture_fill((AVPicture*)picture, picturebuffer, CurrentPixelFormat, CurrentWidth, CurrentHeight);
		return picture;
	};
	void VideoStream::ResettoBegining()
	{
		if (HasVideo) av_seek_frame(FormatContext, VideoStreamID, 0, AVSEEK_FLAG_ANY);
		if (HasAudio) av_seek_frame(FormatContext, AudioStreamID, 0, AVSEEK_FLAG_ANY);
		if (HasVideo) avcodec_flush_buffers(VideoCodecContext);
		if (HasAudio) avcodec_flush_buffers(AudioCodecContext);
		EndOfFile = false;
	}
	void VideoStream::SetPlayingOffset(int64_t MSPosition)
	{
		AVRational avtimebase = {1, AV_TIME_BASE};
		if (HasVideo)
		{
			AVRational timebase = FormatContext->streams[VideoStreamID]->time_base;
			float ftb = (float)timebase.den / (float)timebase.num;
			int64_t pos = (int64_t)((float)MSPosition / 1000.0f * ftb);
			av_seek_frame(FormatContext, VideoStreamID, pos, AVSEEK_FLAG_ANY);
		}
		if (HasAudio)
		{
			AVRational timebase = FormatContext->streams[AudioStreamID]->time_base;
			float ftb = (float)timebase.den / (float)timebase.num;
			int64_t pos = (int64_t)((float)MSPosition / 1000.0f * ftb);
			av_seek_frame(FormatContext, AudioStreamID, pos, AVSEEK_FLAG_ANY);
		}
	}
	DataFrame* VideoStream::DecodeNextDataFrame()
	{
		if (!EndOfFile)
		{
			bool validpacket = false;
			DataFrame* newframe = nullptr;
			while(!validpacket)
			{
				AVPacket* packet(nullptr);
				packet = (AVPacket*)av_malloc(sizeof(*packet));
				av_init_packet(packet);
				if (av_read_frame(FormatContext, packet) == 0)
				{
					if (packet->stream_index == VideoStreamID && HasVideo)
					{
						int DidDecode = 0;
						if (avcodec_decode_video2(VideoCodecContext, VideoRawBuffer, &DidDecode, packet) >= 0)
						{
							if (DidDecode)
							{
								sws_scale(VideoSWContext, VideoRawBuffer->data, VideoRawBuffer->linesize, 0, VideoCodecContext->height, VideoRGBABuffer->data, VideoRGBABuffer->linesize);
								validpacket = true;
								newframe = new DataFrame(VideoRGBABuffer->data[0], VideoCodecContext->width, VideoCodecContext->height);
							}
						}
						else
						{
							// not sure how to handle a failed decode
						}
					}
					else if(packet->stream_index == AudioStreamID && HasAudio)
					{
						int DidDecode = 0;
						if (avcodec_decode_audio4(AudioCodecContext, AudioRawBuffer, &DidDecode, packet) > 0)
						{
							if (DidDecode)
							{
								int conlength = swr_convert(AudioSWContext, &AudioPCMBuffer, AudioRawBuffer->nb_samples, (const uint8_t**)AudioRawBuffer->extended_data, AudioRawBuffer->nb_samples);
								if (conlength > 0)
								{
									validpacket = true;
									newframe = new DataFrame(AudioPCMBuffer, conlength, OutputChannelCount, AudioCodecContext->sample_rate);
								}
								else
								{
									// not sure how to handle a failed conversion
									// possibly fill with blank audio upto the length of the decoded samples before conversion
								}
							}
						}
						else
						{
							// not sure how to handle a failed decode
							// somehow we want to return blank audio data
						}
					}
					av_free_packet(packet);
					av_free(packet);
				}
				else
				{
					EndOfFile = true;
					validpacket = true;
					av_free_packet(packet);
					av_free(packet);
				}
			}
			return newframe;
		}
		return nullptr;
	};
	float VideoStream::GetVideoFPMS()
	{
		if (!HasVideo) return 0;
		AVRational r1 = FormatContext->streams[VideoStreamID]->avg_frame_rate;
		AVRational r2 = FormatContext->streams[VideoStreamID]->r_frame_rate;
		if ((!r1.num || !r1.den) && (!r2.num || !r2.den))
		{
			return (1.0f / 29.97f * 1000.0f);
		}
		else
		{
			if (r2.num && r1.den)
			{
				return ((1.0f / ((float)r1.num / (float)r1.den)) * 1000.0f);
			}
			else
			{
				return ((1.0f / ((float)r2.num / (float)r2.den)) * 1000.0f);
			}
		}
		return (1.0f / 29.97f * 1000.0f);
	};
	bool VideoStream::GetEndOfFile()
	{
		return EndOfFile;
	};
	bool VideoStream::GetHasVideo()
	{
		return HasVideo;
	}
	bool VideoStream::GetHasAudio()
	{
		return HasAudio;
	}
	int64_t VideoStream::GetDuration()
	{
		return (long)VideoLength;
	};
	uint32_t VideoStream::GetVideoHeight()
	{
		if (!HasVideo) return 0;
		return VideoCodecContext->height;
	};
	uint32_t VideoStream::GetVideoWidth()
	{
		if (!HasVideo) return 0;
		return VideoCodecContext->width;
	};
	uint32_t VideoStream::GetAudioChannelCount()
	{
		if (!HasAudio) return 0;
		return OutputChannelCount;
	};
	uint32_t VideoStream::GetAudioSampleRate()
	{
		if (!HasAudio) return 0;
		return AudioCodecContext->sample_rate;
	};
#endif