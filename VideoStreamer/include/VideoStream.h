#ifndef VIDEOSTREAM_H
#define VIDEOSTREAM_H

extern "C"
{
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavutil/opt.h>
}

#include <iostream>
#include "DataFrame.h"

#define MAXSAMPLES 192000 // Max Audio Samples

class VideoStream
{
private:
	bool HasVideo; // Flag if the file supports Video
	bool HasAudio; // Flag if the file supports Audio
	bool EndOfFile; // Flag if reading has reached the end of the file
	int VideoStreamID; // Index of the Video stream
	int AudioStreamID; // Index of the Audio stream
	int64_t VideoLength; // Length of the entire video/audio file
	AVFormatContext* FormatContext; // File format context provides stream information
	AVCodecContext* VideoCodecContext; // Video codec context provides video stream information
	AVCodecContext* AudioCodecContext; // Audio codec context provides audio stream information
	AVCodec* VideoCodec; // Video codec for decoding video
	AVCodec* AudioCodec; // Audio codec for decoding audio
	AVFrame* VideoRawBuffer; // Decoded video frame in input format
	AVFrame* VideoRGBABuffer; // Decoded video frame in RGBA format
	AVFrame* AudioRawBuffer; // Decoded audio frame in input format
	uint8_t* AudioPCMBuffer; // Decoded audio frame in Signed 16 Byte (PCM) format
	SwsContext* VideoSWContext; // Video converter for switching from the input video format to the output RGBA format
	SwrContext* AudioSWContext; // Audio converter for switching from the input audio format to the output Signed 16 Byte (PCM) format
	int OutputChannelCount; // Audio output channel count

public:
	VideoStream(const char* FilePath, int32_t* ReturnValue, bool ForceOptions, bool EnableVideo, bool EnableAudio); // Contructor for opening a VideoStream
	~VideoStream(); // Destructor
	void ResettoBegining(); // Resets the video and audio streams to the begining
	void SetPlayingOffset(int64_t MSPosition); // Seeks the video and audio streams to the specified time offset (in milliseconds)
	DataFrame* DecodeNextDataFrame(); // Decodes and returns the next data frame containing video or audio data
	float GetVideoFPMS(); // Returns the time (in milliseconds) before a new frame is to be displayed
	bool GetHasVideo(); // Returns the flag if the stream has Video
	bool GetHasAudio(); // Returns the flag if the stream has Audio
	bool GetEndOfFile(); // Returns a flag if the stream has reached the end of the file stream
	int64_t GetDuration(); // Returns the length of the steam
	uint32_t GetVideoHeight(); // Returns the height of the video stream
	uint32_t GetVideoWidth(); // Returns the width of the video stream
	uint32_t GetAudioChannelCount(); // Returns the channel count of the audio stream
	uint32_t GetAudioSampleRate(); // Returns the sample rate of the audio stream

private:
	AVFrame* CreateVideoBuffer(enum PixelFormat CurrentPixelFormat, int CurrentWidth, int CurrentHeight); // Creates a AVFrame as a video buffer

};

#endif