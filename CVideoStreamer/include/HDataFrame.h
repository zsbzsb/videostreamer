#ifndef HDATAFRAME_H
#define HDATAFRAME_H

#define exported __declspec(dllexport)

#include "DataFrame.h"

extern "C"
{
	struct HDataFrame;
	typedef struct HDataFrame HDataFrame;
	exported HDataFrame* CreateVideoFrame(uint32_t FrameWidth, uint32_t FrameHeight, uint8_t R, uint8_t G, uint8_t B, uint8_t A);
	exported void DeleteDataFrame(HDataFrame* CurrentDataFrame);
	exported uint8_t* GetVideoDataBuffer(HDataFrame* CurrentDataFrame);
	exported int16_t* GetAudioDataBuffer(HDataFrame* CurrentDataFrame);
	exported uint32_t GetBufferSize(HDataFrame* CurrentDataFrame);
	exported bool GetIsVideoData(HDataFrame* CurrentDataFrame);
	exported bool GetIsAudioData(HDataFrame* CurrentDataFrame);
	exported uint32_t GetFrameWidth(HDataFrame* CurrentDataFrame);
	exported uint32_t GetFrameHeight(HDataFrame* CurrentDataFrame);
	exported uint32_t GetFrameChannelCount(HDataFrame* CurrentDataFrame);
	exported uint32_t GetFrameSampleRate(HDataFrame* CurrentDataFrame);
}

#endif