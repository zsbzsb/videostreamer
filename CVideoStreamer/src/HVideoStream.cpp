#ifndef HVIDEOSTREAM_CPP
#define HVIDEOSTREAM_CPP

#include "include/HVideoStream.h"

HVideoStream* CreateVideoStream(const char* FilePath, int32_t* ReturnValue, bool ForceOptions, bool EnableVideo, bool EnableAudio)
{
	return reinterpret_cast<HVideoStream*>(new VideoStream(FilePath, ReturnValue, ForceOptions, EnableVideo, EnableAudio));
};
void DeleteVideoStream(HVideoStream* CurrentVideoStream)
{
	delete reinterpret_cast<VideoStream*>(CurrentVideoStream);
};
void ResettoBegining(HVideoStream* CurrentVideoStream)
{
	reinterpret_cast<VideoStream*>(CurrentVideoStream)->ResettoBegining();
};
void SetPlayingOffset(HVideoStream* CurrentVideoStream, int64_t MSPosition)
{
	reinterpret_cast<VideoStream*>(CurrentVideoStream)->SetPlayingOffset(MSPosition);
};
HDataFrame* DecodeNextDataFrame(HVideoStream* CurrentVideoStream)
{
	return reinterpret_cast<HDataFrame*>(reinterpret_cast<VideoStream*>(CurrentVideoStream)->DecodeNextDataFrame());
};
float GetVideoFPMS(HVideoStream* CurrentVideoStream)
{
	return reinterpret_cast<VideoStream*>(CurrentVideoStream)->GetVideoFPMS();
};
bool GetEndOfFile(HVideoStream* CurrentVideoStream)
{
	return reinterpret_cast<VideoStream*>(CurrentVideoStream)->GetEndOfFile();
};
bool GetHasVideo(HVideoStream* CurrentVideoStream)
{
	return reinterpret_cast<VideoStream*>(CurrentVideoStream)->GetHasVideo();
};
bool GetHasAudio(HVideoStream* CurrentVideoStream)
{
	return reinterpret_cast<VideoStream*>(CurrentVideoStream)->GetHasAudio();
};
int64_t GetDuration(HVideoStream* CurrentVideoStream)
{
	return reinterpret_cast<VideoStream*>(CurrentVideoStream)->GetDuration();
};
uint32_t GetVideoHeight(HVideoStream* CurrentVideoStream)
{
	return reinterpret_cast<VideoStream*>(CurrentVideoStream)->GetVideoHeight();
};
uint32_t GetVideoWidth(HVideoStream* CurrentVideoStream)
{
	return reinterpret_cast<VideoStream*>(CurrentVideoStream)->GetVideoWidth();
};
uint32_t GetAudioChannelCount(HVideoStream* CurrentVideoStream)
{
	return reinterpret_cast<VideoStream*>(CurrentVideoStream)->GetAudioChannelCount();
};
uint32_t GetAudioSampleRate(HVideoStream* CurrentVideoStream)
{
	return reinterpret_cast<VideoStream*>(CurrentVideoStream)->GetAudioSampleRate();
};

#endif