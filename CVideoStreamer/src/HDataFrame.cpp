#ifndef HDATAFRAME_CPP
#define HDATAFRAME_CPP

#include "include/HDataFrame.h"

HDataFrame* CreateVideoFrame(uint32_t FrameWidth, uint32_t FrameHeight, uint8_t R, uint8_t G, uint8_t B, uint8_t A)
{
	return reinterpret_cast<HDataFrame*>(new DataFrame(FrameWidth, FrameHeight, R, G, B, A));
};
void DeleteDataFrame(HDataFrame* CurrentDataFrame)
{
	delete reinterpret_cast<DataFrame*>(CurrentDataFrame);
};
uint8_t* GetVideoDataBuffer(HDataFrame* CurrentDataFrame)
{
	return reinterpret_cast<DataFrame*>(CurrentDataFrame)->VideoDataBuffer;
}
int16_t* GetAudioDataBuffer(HDataFrame* CurrentDataFrame)
{
	return reinterpret_cast<DataFrame*>(CurrentDataFrame)->AudioDataBuffer;
}
uint32_t GetBufferSize(HDataFrame* CurrentDataFrame)
{
	return reinterpret_cast<DataFrame*>(CurrentDataFrame)->BufferSize;
}
bool GetIsVideoData(HDataFrame* CurrentDataFrame)
{
	return reinterpret_cast<DataFrame*>(CurrentDataFrame)->IsVideoData;
}
bool GetIsAudioData(HDataFrame* CurrentDataFrame)
{
	return reinterpret_cast<DataFrame*>(CurrentDataFrame)->IsAudioData;
}
uint32_t GetFrameWidth(HDataFrame* CurrentDataFrame)
{
	return reinterpret_cast<DataFrame*>(CurrentDataFrame)->FrameWidth;
}
uint32_t GetFrameHeight(HDataFrame* CurrentDataFrame)
{
	return reinterpret_cast<DataFrame*>(CurrentDataFrame)->FrameHeight;
}
uint32_t GetFrameChannelCount(HDataFrame* CurrentDataFrame)
{
	return reinterpret_cast<DataFrame*>(CurrentDataFrame)->AudioChannelCount;
}
uint32_t GetFrameSampleRate(HDataFrame* CurrentDataFrame)
{
	return reinterpret_cast<DataFrame*>(CurrentDataFrame)->AudioSampleRate;
}

#endif