﻿using System;
using System.Threading;
using System.Collections.Concurrent;
using SFML.Window;
using SFML.Graphics;
using NetEXT.TimeFunctions;
using NetEXT.Graphics;

namespace VideoStreamerNET
{
    public enum PlayState
    {
        Stopped = 1,
        Playing = 2,
        Paused = 3
    }
    /// <summary>Class for managing loading and playing a video</summary>
    public class VideoStream : IDisposable
    {
        #region Consts
        /// <summary>The default time offset before audio data will be skipped/padded to match the current playing offset</summary>
        public static readonly Time DefaultAudioSyncTime = Time.FromMilliseconds(300);
        #endregion

        #region Variables
        private IntPtr _handle = IntPtr.Zero;
        private PlayState _state = PlayState.Stopped;
        private Thread _decodethread = null;
        private bool _shouldthreadrun = true;
        private bool _isdecoding = false;
        private bool _preload = false;
        private bool _isplayingtoeof = false;
        private bool _iseof = false;
        private bool _hasvideo = false;
        private bool _hasaudio = false;
        private uint _videoframequeueamount = 10;
        private uint _audioframequeueamount = 10;
        private Vector2u _videosize = new Vector2u(1, 1);
        private ConcurrentQueue<VideoFrame> _queuedvideoframes = null;
        private ConcurrentQueue<AudioFrame> _queuedaudioframes = null;
        private AudioStream _currentaudiostream = null;
        private VideoFrame _currentvideoframe = null;
        private Time _videoframejumptime = Time.Zero;
        private Time _videoelapsedtime = Time.Zero;
        private Time _totalelapsedtime = Time.Zero;
        private int _jumpvideoframecount = 0;
        private Color _buffercolor = Color.Black;
        private bool _hardstop = false;
        private bool _disposed = false;
        #endregion

        #region Properties
        /// <summary>Flag to determine if video is available in this stream.</summary>
        public bool CanPlayVideo
        {
            get
            {
                return _hasvideo;
            }
        }
        /// <summary>Flag to determine if audio is available in this stream.</summary>
        public bool CanPlayAudio
        {
            get
            {
                return _hasaudio;
            }
        }
        /// <summary>Gets the size of the video being played.</summary>
        public Vector2u VideoSize
        {
            get
            {
                return _videosize;
            }
        }
        /// <summary>The current video frame that is to be displayed.</summary>
        public VideoFrame VideoFrameData
        {
            get
            {
                return _currentvideoframe;
            }
        }
        /// <summary>The current state of this stream.</summary>
        public PlayState CurrentState
        {
            get
            {
                return _state;
            }
        }
        /// <summary>The total length of this stream.</summary>
        public Time VideoLength
        {
            get
            {
                return Time.FromMilliseconds(Imports.GetDuration(_handle) / 1000);
            }
        }
        /// <summary>The current playing offset of this stream.</summary>
        public Time VideoPlayingOffset
        {
            get
            {
                return _totalelapsedtime;
            }
            set
            {
                if (value <= VideoLength)
                {
                    bool startplaying = (_state == PlayState.Playing);
                    Stop();
                    _totalelapsedtime = value;
                    Imports.SetPlayingOffset(_handle, value.Milliseconds);
                    if (startplaying) Play();
                }
            }
        }
        /// <summary>The initial color of the texture buffer.</summary>
        public Color BufferColor
        {
            get
            {
                return _buffercolor;
            }
            set
            {
                _buffercolor = value;
                if (_hasvideo && _state == PlayState.Stopped)
                {
                    if (_currentvideoframe != null)
                    {
                        _currentvideoframe.Dispose();
                        _currentvideoframe = null;
                    }
                    _currentvideoframe = new VideoFrame(Imports.CreateVideoFrame(_videosize.X, _videosize.Y, _buffercolor.R, _buffercolor.G, _buffercolor.B, _buffercolor.A));
                    if (FrameChanged != null) FrameChanged(this);
                }
            }
        }
        /// <summary>Flag to determine if frames should be decoded when the video is not playing.</summary>
        public bool Preload
        {
            get
            {
                return _preload;
            }
            set
            {
                _preload = value;
            }
        }
        /// <summary>Flag to determine if video playback should be halted as soon as the playing offset is greater than the video length.</summary>
        public bool HardStop
        {
            get
            {
                return _hardstop;
            }
            set
            {
                _hardstop = value;
            }
        }
        /// <summary>Gets or sets the volume of the internal audio source.</summary>
        public float Volume
        {
            get
            {
                return _currentaudiostream != null ? _currentaudiostream.Volume : 0;
            }
            set
            {
                _currentaudiostream.Volume = value;
            }
        }
        #endregion

        #region Events
        /// <summary>The event is triggered when switching to a new video frame</summary>
        public event Action<VideoStream> FrameChanged;
        /// <summary>The event is triggered when the end of the stream has been reached</summary>
        public event Action<VideoStream> EndofFileReached;
        #endregion

        #region Constructors/Destructors
        /// <summary>Constructor for a VideoStream, all other parameters will have default values</summary>
        /// <param name="FilePath">The path to the video file to load</param>
        public VideoStream(string FilePath) : this(FilePath, DefaultAudioSyncTime, Color.Black) { }
        /// <summary>Constructor for a VideoStream, all other parameters will have default values</summary>
        /// <param name="FilePath">The path to the video file to load</param>
        /// <param name="BufferColor">The initial color of the texture buffer</param>
        public VideoStream(string FilePath, Color BufferColor) : this(FilePath, DefaultAudioSyncTime, BufferColor) { }
        /// <summary>Constructor for a VideoStream, all parameters have the option of being overriden from the default values</summary>
        /// <param name="FilePath">The path to the video file to load</param>
        /// <param name="AudioSyncTime">The time offset before audio data will be skipped/padded to match the current playing offset</param>
        /// <param name="BufferColor">The initial color of the texture buffer</param>
        /// <param name="EnableVideo">A flag to determine if video data should be decoded and buffered for this stream</param>
        /// <param name="EnableAudio">A flag to determine if audio data should be decoded and buffered for this stream</param>
        /// <param name="ForceOptions">A flag that if true will result in an exception if EnableVideo/EnableAudio is true, but the stream file does not support either option</param>
        /// <param name="VideoFrameQueueAmount">The amount of video frames to buffer before pausing the decoder</param>
        /// <param name="AudioFrameQueueAmount">The amount of audio frames to buffer before pausing the decoder</param>
        public VideoStream(string FilePath, Time AudioSyncTime, Color BufferColor, bool EnableVideo = true, bool EnableAudio = true, bool ForceOptions = false, uint VideoFrameQueueAmount = 10, uint AudioFrameQueueAmount = 10)
        {
            _buffercolor = BufferColor;
            Int32 returnvalue = 0;
            unsafe
            {
                _handle = Imports.CreateVideoStream(FilePath, &returnvalue, ForceOptions, EnableVideo, EnableAudio);
            }
            if (returnvalue != 0)
            {
                Imports.DeleteVideoStream(_handle);
                _handle = IntPtr.Zero;
                HandleCreateError(returnvalue);
            }
            else
            {
                _hasvideo = Imports.GetHasVideo(_handle);
                _hasaudio = Imports.GetHasAudio(_handle);
                if (_hasvideo)
                {
                    _videosize = new Vector2u(Imports.GetVideoWidth(_handle), Imports.GetVideoHeight(_handle));
                    _queuedvideoframes = new ConcurrentQueue<VideoFrame>();
                    _videoframejumptime = Time.FromSeconds(Imports.GetVideoFPMS(_handle) / 1000f);
                    _videoframequeueamount = VideoFrameQueueAmount;
                    _currentvideoframe = new VideoFrame(Imports.CreateVideoFrame(_videosize.X, _videosize.Y, _buffercolor.R, _buffercolor.G, _buffercolor.B, _buffercolor.A));
                }
                if (_hasaudio)
                {
                    _queuedaudioframes = new ConcurrentQueue<AudioFrame>();
                    _currentaudiostream = new AudioStream(Imports.GetAudioChannelCount(_handle), Imports.GetAudioSampleRate(_handle), _queuedaudioframes, AudioSyncTime);
                    _audioframequeueamount = AudioFrameQueueAmount;
                }
                _decodethread = new Thread(new ThreadStart(DecodeThreadEntry));
                _decodethread.Start();
            }
        }
        ~VideoStream()
        {
            Dispose();
        }
        /// <summary>Will dispose this stream and any other unmanaged resources that are owned</summary>
        public void Dispose()
        {
            if (!_disposed)
            {
                _disposed = true;
                _shouldthreadrun = false;
                if (_decodethread.IsAlive) _decodethread.Join();
                Imports.DeleteVideoStream(_handle);
                if (_currentvideoframe != null) _currentvideoframe.Dispose();
                _currentvideoframe = null;
                if (_currentaudiostream != null) _currentaudiostream.Dispose();
                _currentaudiostream = null;
                if (_hasvideo)
                {
                    while (_queuedvideoframes.Count > 0)
                    {
                        VideoFrame frame = null;
                        if (_queuedvideoframes.TryDequeue(out frame))
                        {
                            frame.Dispose();
                            frame = null;
                        }
                    }
                }
                if (_hasaudio)
                {
                    while (_queuedaudioframes.Count > 0)
                    {
                        AudioFrame frame = null;
                        if (_queuedaudioframes.TryDequeue(out frame))
                        {
                            frame.Dispose();
                            frame = null;
                        }
                    }
                }
            }
        }
        #endregion

        #region Decode Thread
        private void DecodeThreadEntry()
        {
            while (_shouldthreadrun)
            {
                if (_isdecoding || _preload)
                {
                    bool queuevideo = false;
                    bool queueaudio = false;
                    if (_hasvideo) queuevideo = (_queuedvideoframes.Count < _videoframequeueamount && !_isplayingtoeof);
                    if (_hasaudio) queueaudio = (_queuedaudioframes.Count < _audioframequeueamount && !_isplayingtoeof);
                    if (queuevideo || queueaudio)
                    {
                        IntPtr newframehandle = Imports.DecodeNextDataFrame(_handle);
                        if (newframehandle != IntPtr.Zero)
                        {
                            // got frame
                            // check to determine if it is audio or video
                            bool isvideo = Imports.GetIsVideoData(newframehandle);
                            bool isaudio = Imports.GetIsAudioData(newframehandle);
                            if (isvideo && _hasvideo)
                            {
                                VideoFrame newframe = new VideoFrame(newframehandle);
                                _queuedvideoframes.Enqueue(newframe);
                            }
                            else if (isaudio && _hasaudio)
                            {
                                AudioFrame newframe = new AudioFrame(newframehandle);
                                _queuedaudioframes.Enqueue(newframe);
                            }
                            else
                            {
                                Imports.DeleteDataFrame(newframehandle);
                            }
                        }
                        else
                        {
                            // no frame returned
                            // check for eof
                            if (Imports.GetEndOfFile(_handle))
                            {
                                _isdecoding = false;
                                Imports.ResettoBegining(_handle);
                                _isplayingtoeof = true;
                                if (_hasaudio) _currentaudiostream.PlayingtoEof = true;
                            }
                        }
                    }
                    else
                    {
                        Thread.Sleep(5);
                    }
                }
                else
                {
                    Thread.Sleep(5);
                }
            }
        }
        #endregion

        #region Functions
        /// <summary>Updates the playing video and audio buffers. This call is required while the stream is playing</summary>
        public void Update(Time DeltaTime)
        {
            if (_state == PlayState.Playing)
            {
                _totalelapsedtime += DeltaTime;
                if (_hasvideo) _videoelapsedtime += DeltaTime;
                if (_hasaudio) _currentaudiostream.Update(DeltaTime);
            }
            if (_hasvideo)
            {
                while (_videoelapsedtime > _videoframejumptime)
                {
                    _videoelapsedtime -= _videoframejumptime;
                    _jumpvideoframecount += 1;
                }
                while (_queuedvideoframes.Count > 0 && _jumpvideoframecount > 0)
                {
                    VideoFrame frame = null;
                    if (_queuedvideoframes.TryDequeue(out frame))
                    {
                        if (frame.VideoBufferSize > 0)
                        {
                            _currentvideoframe.Dispose();
                            _currentvideoframe = null;
                            _currentvideoframe = frame;
                            if (_queuedvideoframes.Count == 1 || _jumpvideoframecount == 1)
                            {
                                if (FrameChanged != null) FrameChanged(this);
                            }
                        }
                        else
                        {
                            frame.Dispose();
                            frame = null;
                        }
                        _jumpvideoframecount -= 1;
                    }
                }
                if (_queuedvideoframes.Count == 0 && _isplayingtoeof && _state == PlayState.Playing)
                {
                    _iseof = true;
                }
            }
            if (((_hasvideo ? _iseof : true) && (_hasaudio ? _currentaudiostream.IsEof : true)) || (_hardstop && VideoPlayingOffset > VideoLength))
            {
                Stop();
                if (EndofFileReached != null) EndofFileReached(this);
            }
        }
        /// <summary>Starts playing the current stream. No effect if the stream is already playing.</summary>
        public void Play()
        {
            if (_state != PlayState.Playing)
            {
                if (_state == PlayState.Stopped)
                {
                    _isplayingtoeof = false;
                    if (_hasaudio) _currentaudiostream.PlayingtoEof = false;
                    _jumpvideoframecount = 1;
                }
                if (_hasaudio) _currentaudiostream.Play();
                _state = PlayState.Playing;
                _isdecoding = true;
            }
        }
        /// <summary>Pauses the current stream. No effect if the stream is not playing.</summary>
        public void Pause()
        {
            if (_state == PlayState.Playing)
            {
                _state = PlayState.Paused;
                if (_hasaudio) _currentaudiostream.Pause();
                _isdecoding = false;
            }
        }
        /// <summary>Stops the current stream and resets the playing offset to the begining. No effect if the stream is already stopped.</summary>
        public void Stop()
        {
            if (_state != PlayState.Stopped)
            {
                _state = PlayState.Stopped;
                _isplayingtoeof = false;
                _iseof = false;
                if (_hasaudio) _currentaudiostream.PlayingtoEof = false;
                if (_hasaudio) _currentaudiostream.Stop();
                if (_hasaudio) _currentaudiostream.IsEof = false;
                _isdecoding = false;
                _videoelapsedtime = Time.Zero;
                _totalelapsedtime = Time.Zero;
                _jumpvideoframecount = 0;
                Imports.ResettoBegining(_handle);
                if (_hasaudio) _currentaudiostream.ResettoBegining();
                if (_hasvideo)
                {
                    while (_queuedvideoframes.Count > 0)
                    {
                        VideoFrame frame = null;
                        if (_queuedvideoframes.TryDequeue(out frame))
                        {
                            frame.Dispose();
                            frame = null;
                        }
                    }
                }
                if (_hasaudio)
                {
                    while (_queuedaudioframes.Count > 0)
                    {
                        AudioFrame frame = null;
                        if (_queuedaudioframes.TryDequeue(out frame))
                        {
                            frame.Dispose();
                            frame = null;
                        }
                    }
                }
                if (_hasvideo)
                {
                    _currentvideoframe.Dispose();
                    _currentvideoframe = null;
                    _currentvideoframe = new VideoFrame(Imports.CreateVideoFrame(_videosize.X, _videosize.Y, _buffercolor.R, _buffercolor.G, _buffercolor.B, _buffercolor.A));
                    if (FrameChanged != null) FrameChanged(this);
                }
            }
        }
        #endregion

        #region Error Handling
        private void HandleCreateError(int ErrorCode)
        {
            if (ErrorCode == 1)
            {
                throw new Exception("Unable to load specified file");
            }
            else if (ErrorCode == 2)
            {
                throw new Exception("Unable to find the stream information");
            }
            else if (ErrorCode == 3)
            {
                throw new Exception("Unable to find a valid video stream");
            }
            else if (ErrorCode == 4)
            {
                throw new Exception("Unable to find a valid audio stream");
            }
            else if (ErrorCode == 5)
            {
                throw new Exception("Unable to get the video codec context");
            }
            else if (ErrorCode == 6)
            {
                throw new Exception("Unable to find a video codec");
            }
            else if (ErrorCode == 7)
            {
                throw new Exception("Unable to get the audio codec context");
            }
            else if (ErrorCode == 8)
            {
                throw new Exception("Unable to find an audio codec");
            }
            else if (ErrorCode == 9)
            {
                throw new Exception("Unable to load video codec");
            }
            else if (ErrorCode == 10)
            {
                throw new Exception("Unable to load audio codec");
            }
            else if (ErrorCode == 11)
            {
                throw new Exception("Unable to create video buffers");
            }
            else if (ErrorCode == 12)
            {
                throw new Exception("Unable to create audio buffers");
            }
            else if (ErrorCode == 13)
            {
                throw new Exception("Unable to load audio or video");
            }
        }
        #endregion
    }
}
