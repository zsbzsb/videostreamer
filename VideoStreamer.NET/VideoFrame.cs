﻿using System;

namespace VideoStreamerNET
{
    /// <summary>Represents a single frame of a video</summary>
    public class VideoFrame : IDisposable
    {
        #region Variables
        private IntPtr _handle = IntPtr.Zero;
        private VideoBuffer _currentvideobuffer = null;
        private bool _disposed = false;
        #endregion

        #region Properties
        /// <summary>Returns the video data of this frame</summary>
        public VideoBuffer VideoDataBuffer
        {
            get
            {
                return _currentvideobuffer;
            }
        }
        internal UInt32 VideoBufferSize
        {
            get
            {
                return Imports.GetBufferSize(_handle);
            }
        }
        /// <summary>Returns the width of this frame</summary>
        public UInt32 FrameWidth
        {
            get
            {
                return Imports.GetFrameWidth(_handle);
            }
        }
        /// <summary>Returns the height of this frame</summary>
        public UInt32 FrameHeight
        {
            get
            {
                return Imports.GetFrameHeight(_handle);
            }
        }
        #endregion

        #region Constructors/Destructors
        internal VideoFrame(IntPtr VideoFrameHandle)
        {
            _handle = VideoFrameHandle;
            _currentvideobuffer = new VideoBuffer(VideoFrameHandle);
        }
        ~VideoFrame()
        {
            Dispose();
        }
        /// <summary>Will dispose this frame and any other unmanaged resources that are owned</summary>
        public void Dispose()
        {
            if (!_disposed)
            {
                _disposed = true;
                _currentvideobuffer = null;
                Imports.DeleteDataFrame(_handle);
            }
        }
        #endregion
    }
}
