﻿using System;
using SFML.Window;
using SFML.Graphics;
using NetEXT.Graphics;

namespace VideoStreamerNET
{
    /// <summary>Specialized wrapper around a sprite to provide an easy way to render a <see cref="VideoStreamerNET.VideoStream"/>.</summary>
    public class VideoDrawer : Transformable, Drawable, IDisposable
    {
        #region Variables
        private VideoStream _stream = null;
        private LargeTexture _texture = null;
        private LargeSprite _sprite = null;
        private bool _disposed = false;
        #endregion

        #region Properties
        /// <summary>Gets or sets the stream that will be rendered.</summary>
        public VideoStream Stream
        {
            get
            {
                return _stream;
            }
            set
            {
                _stream.FrameChanged -= FrameChanged;
                _stream = value;
                _sprite.Dispose();
                _sprite = null;
                _texture.Dispose();
                _texture = null;
                _texture = new LargeTexture(_stream.VideoSize.X, _stream.VideoSize.Y);
                _sprite = new LargeSprite(_texture);
                _stream.FrameChanged += FrameChanged;
            }
        }
        #endregion

        #region Constructors
        /// <summary>Constructs a new video drawer with the specified stream.</summary>
        /// <param name="Stream">Stream that will be rendered</param>
        public VideoDrawer(VideoStream Stream)
        {
            _stream = Stream;
            if (_stream.VideoFrameData != null)
            {
                _texture = new LargeTexture(_stream.VideoSize.X, _stream.VideoSize.Y);
                _texture.Update(Stream.VideoFrameData.VideoDataBuffer.ToArray());
                _sprite = new LargeSprite(_texture);
                _stream.FrameChanged += FrameChanged;
            }
        }
        #endregion

        #region Functions
        private void FrameChanged(VideoStream Stream)
        {
            if (_stream.VideoFrameData != null)
            {
                _texture.Update(Stream.VideoFrameData.VideoDataBuffer.ToArray());
            }
        }
        public void Draw(RenderTarget Target, RenderStates States)
        {
            if (_stream.VideoFrameData != null)
            {
                States.Transform *= base.Transform;
                Target.Draw(_sprite, States);
            }
        }
        #endregion
    }
}
