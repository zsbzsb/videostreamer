VideoStreamer -- This software is provided 'as-is' without any express or
implied warranty. Read the full license agreement in "license.txt".

VideoStreamer was created by Zachariah Brown (zsbzsb) and Copyrighted (C) 2013 by Brown Star. VideoStreamer is written in C++ with a C binding to .NET as a bridge between FFMPEG video decoding and SFML rendering.